## Arquivo de Pedido
gerador-layouts arquivoDePedido header NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoCliente:string:6:20 DataGeracao:int32:20:28 HoraGeracao:int32:28:32 NrPedido:int32:32:38 PedidoConsulta:int32:38:39 FormaPagamento:string:39:43

gerador-layouts arquivoDePedido produto NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoProduto:int64:6:19 Fixo:int64:19:32 Quantidade:int32:32:36 Filler:int32:36:40

gerador-layouts arquivoDePedido trailler NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 QuantidadeTipo2:int32:6:10 Filler:int32:10:40


## Arquivo de Retorno
gerador-layouts retornoDePedido header NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoCliente:string:6:20 DataGeracao:int32:20:28 HoraGeracao:int32:28:32 NrPedido:int32:32:38 PedidoConsulta:int32:38:39 NrPedidoFOrnecedor:string:39:49

gerador-layouts retornoDePedido produto NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 CodigoProduto:int64:6:19 ValorUnitario:float64:19:32:2 Quantidade:int32:32:36 PercentualDesconto:float64:36:45:2

gerador-layouts retornoDePedido trailler NumeroSequencial:int32:0:4 TipoRegistro:string:4:6 Fixo:string:6:16 ValorBruto:float64:16:26:2 ValorDesconto:float64:26:36:2 Filler:int32:36:40