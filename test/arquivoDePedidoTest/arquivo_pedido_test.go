package arquivoDePedidoTest

import (
	"layout-dosecerta/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {

	f, err := os.Open("../fileTest/mock.txt")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var header arquivoDePedido.Header
	//2222013333333333333344444444555566666678888
	header.NumeroSequencial = 2222
	header.TipoRegistro = "01"
	header.CodigoCliente = "33333333333333"
	header.DataGeracao = 44444444
	header.HoraGeracao = 5555
	header.NrPedido = 666666
	header.PedidoConsulta = 7
	header.FormaPagamento = "8888"

	if header.NumeroSequencial != arq.Header.NumeroSequencial {
		t.Error("NumeroSequencial não é compativel")
	}
	if header.TipoRegistro != arq.Header.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if header.CodigoCliente != arq.Header.CodigoCliente {
		t.Error("CodigoCliente não é compativel")
	}
	if header.DataGeracao != arq.Header.DataGeracao {
		t.Error("DataGeracao não é compativel")
	}
	if header.HoraGeracao != arq.Header.HoraGeracao {
		t.Error("HoraGeracao não é compativel")
	}
	if header.NrPedido != arq.Header.NrPedido {
		t.Error("NrPedido não é compativel")
	}
	if header.PedidoConsulta != arq.Header.PedidoConsulta {
		t.Error("PedidoConsulta não é compativel")
	}
	if header.FormaPagamento != arq.Header.FormaPagamento {
		t.Error("FormaPagamento não é compativel")
	}

	var Produto arquivoDePedido.Produto
	//2222023333333333333000000000000044440000
	Produto.NumeroSequencial = 2222
	Produto.TipoRegistro  = "02"
	Produto.CodigoProduto = 3333333333333
	Produto.Fixo = 0000000000000
	Produto.Quantidade = 4444
	Produto.Filler =  0000

	if Produto.NumeroSequencial != arq.Produto[0].NumeroSequencial {
		t.Error("NumeroSequencial não é compativel")
	}
	if Produto.TipoRegistro != arq.Produto[0].TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Produto.CodigoProduto != arq.Produto[0].CodigoProduto {
		t.Error("CodigoProduto não é compativel")
	}
	if Produto.Fixo != arq.Produto[0].Fixo {
		t.Error("Fixo não é compativel")
	}
	if Produto.Quantidade != arq.Produto[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}
	if Produto.Filler != arq.Produto[0].Filler {
		t.Error("Filler não é compativel")
	}

	var Trailer arquivoDePedido.Trailler
	//2222034444000000000000000000000000000000

	Trailer.NumeroSequencial = 2222
	Trailer.TipoRegistro = "03"
	Trailer.QuantidadeTipo2 = 4444
	Trailer.Filler  = 000000000000000000000000000000

	if Trailer.NumeroSequencial != arq.Trailler.NumeroSequencial {
		t.Error("NumeroSequencial não é compativel")
	}
	if Trailer.TipoRegistro != arq.Trailler.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Trailer.QuantidadeTipo2 != arq.Trailler.QuantidadeTipo2 {
		t.Error("QuantidadeTipo2 não é compativel")
	}
	if Trailer.Filler != arq.Trailler.Filler {
		t.Error("Filler não é compativel")
	}


}
