package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailler struct {
	NumeroSequencial int32   `json:"NumeroSequencial"`
	TipoRegistro     string  `json:"TipoRegistro"`
	Fixo             string  `json:"Fixo"`
	ValorBruto       float64 `json:"ValorBruto"`
	ValorDesconto    float64 `json:"ValorDesconto"`
	Filler           int32   `json:"Filler"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Fixo, "Fixo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorBruto, "ValorBruto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorDesconto, "ValorDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial": {0, 4, 0},
	"TipoRegistro":     {4, 6, 0},
	"Fixo":             {6, 16, 0},
	"ValorBruto":       {16, 26, 2},
	"ValorDesconto":    {26, 36, 2},
	"Filler":           {36, 40, 0},
}
